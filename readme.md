Hello Futured!

This is showcase I'd like to present to give you an idea how we can create powerful backend with Kotlin and Spring Boot. 

The project structure and all source files in the first commit was generated in a few seconds via [Spring initilizr](start.spring.io) with [this](https://start.spring.io/#!type=maven-project&language=kotlin&platformVersion=2.3.0.M4&packaging=jar&jvmVersion=1.8&groupId=app.futured&artifactId=showcase&name=showcase&description=Showcase%20project%20for%20Spring%20Boot&packageName=app.futured.showcase&dependencies=web,security,data-jpa,liquibase,postgresql,validation) configuration.

Configured dependencies allows us to easily create simple and secured backend application.

I hope you will like it :)
