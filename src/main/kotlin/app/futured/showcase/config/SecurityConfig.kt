package app.futured.showcase.config

import app.futured.showcase.security.AuthFilter
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter

@EnableWebSecurity
class SecurityConfig: WebSecurityConfigurerAdapter() {

    override fun configure(web: WebSecurity) {
        http.addFilterAfter(
            AuthFilter(), BasicAuthenticationFilter::class.java
        )
        // disable caching
        http.headers().cacheControl()
        // enable cors headers
        http.cors()
        // disable csrf for our requests.
        http.csrf().disable()

        http.sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
            // .antMatchers("/").permitAll()
            // .anyRequest().authenticated()
    }
}
