package app.futured.showcase.rest

import app.futured.showcase.service.CandidateService
import app.futured.showcase.service.dto.CandidateDto
import app.futured.showcase.service.dto.CreateCandidateDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("candidate")
class CandidateRest(
    val candidateService: CandidateService
) {

    @PostMapping
    fun createCandidate(@Validated @RequestBody dto: CreateCandidateDto): CandidateDto {
        return candidateService.createCandidate(dto)
    }

    @GetMapping
    fun getCandidates(@PageableDefault pageable: Pageable): Page<CandidateDto> {
        return candidateService.getCandidates(pageable)
    }

    @GetMapping
    @RequestMapping("{id}")
    fun getCandidate(@PathVariable("id") id: Long): CandidateDto {
        return candidateService.getCandidate(id)
    }
}
