package app.futured.showcase.data.repository

import app.futured.showcase.data.entity.Candidate
import org.springframework.data.jpa.repository.JpaRepository

interface CandidateRepository: JpaRepository<Candidate, Long>
