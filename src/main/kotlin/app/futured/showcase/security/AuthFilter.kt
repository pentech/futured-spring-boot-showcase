package app.futured.showcase.security

import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class AuthFilter : GenericFilterBean() {

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val token = (request as HttpServletRequest).getHeader(AUTHORIZATION)
        if (token == "token") {
            SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(
                "Dummy user",
                null,
                AuthorityUtils.NO_AUTHORITIES
            )
        }
        chain.doFilter(request, response)
    }
}
