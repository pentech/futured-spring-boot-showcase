package app.futured.showcase.service.mapping

import app.futured.showcase.data.entity.Candidate
import app.futured.showcase.service.dto.CandidateDto
import app.futured.showcase.service.dto.CreateCandidateDto

fun Candidate.toDto(): CandidateDto {
    return CandidateDto(
        id = id,
        name = name,
        surname = surname,
        phone = phone,
        email = email
    )
}

fun CreateCandidateDto.toEntity(): Candidate {
    return Candidate(
        name = name,
        surname = surname,
        phone = phone,
        email = email
    )
}
