package app.futured.showcase.service.impl

import app.futured.showcase.data.entity.Candidate
import app.futured.showcase.data.repository.CandidateRepository
import app.futured.showcase.service.CandidateService
import app.futured.showcase.service.dto.CandidateDto
import app.futured.showcase.service.dto.CreateCandidateDto
import app.futured.showcase.service.mapping.toDto
import app.futured.showcase.service.mapping.toEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException

@Service
@Transactional
class CandidateServiceImpl(
    private val candidateRepository: CandidateRepository
) : CandidateService {

    override fun createCandidate(createCandidateDto: CreateCandidateDto): CandidateDto {
        val mappedCandidate = createCandidateDto.toEntity()
        val savedCandidate = candidateRepository.save(mappedCandidate)
        return savedCandidate.toDto()
    }

    override fun getCandidate(id: Long): CandidateDto {
        return candidateRepository.findById(id)
            .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
            .toDto()
    }

    override fun getCandidates(pageable: Pageable): Page<CandidateDto> {
        return candidateRepository.findAll(pageable)
            .map { it.toDto() }
    }
}
