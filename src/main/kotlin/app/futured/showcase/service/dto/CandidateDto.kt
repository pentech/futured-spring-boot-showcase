package app.futured.showcase.service.dto

data class CandidateDto(
    val id: Long,
    var name: String,
    var surname: String,
    var email: String,
    var phone: String? = null
)
