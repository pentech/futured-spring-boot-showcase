package app.futured.showcase.service.dto

import javax.validation.constraints.Email
import javax.validation.constraints.Pattern

data class CreateCandidateDto(
    var name: String,
    var surname: String,
    @field:Email
    var email: String,
    @field:Pattern(regexp = """([+]?\d{1,3}[. \s]?)?(\d{9}?)""")
    var phone: String? = null
)
