package app.futured.showcase.service

import app.futured.showcase.service.dto.CandidateDto
import app.futured.showcase.service.dto.CreateCandidateDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface CandidateService {
    fun createCandidate(createCandidateDto: CreateCandidateDto): CandidateDto
    fun getCandidate(id: Long): CandidateDto
    fun getCandidates(pageable: Pageable): Page<CandidateDto>
}
