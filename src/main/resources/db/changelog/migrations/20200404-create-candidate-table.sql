create table candidate (
    id bigserial primary key,
    name text not null,
    surname text not null,
    email text not null,
    phone text,
    created_at timestamp not null,
    updated_at timestamp not null
)
