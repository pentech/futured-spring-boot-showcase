package app.futured.showcase.service.impl

import app.futured.showcase.service.CandidateService
import app.futured.showcase.service.dto.CreateCandidateDto
import org.assertj.core.internal.bytebuddy.utility.RandomString
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class CandidateServiceImplIT(
    @Autowired private val candidateService: CandidateService
) {
    @Test
    fun `creating valid candidate should return created candidate`() {
        val createDto = CreateCandidateDto(
            name = RandomString.make(),
            surname = RandomString.make(),
            email = "${RandomString.make()}@${RandomString.make()}.com"
        )
        val createdCandidate = candidateService.createCandidate(createDto)
        assertEquals(createdCandidate.name, createDto.name)
        assertEquals(createdCandidate.surname, createDto.surname)
        assertEquals(createdCandidate.email, createDto.email)
        assertNull(createdCandidate.phone)
        assertNotNull(createdCandidate.id)
    }
}
