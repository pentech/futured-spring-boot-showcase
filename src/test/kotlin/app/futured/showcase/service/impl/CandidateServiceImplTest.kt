package app.futured.showcase.service.impl

import app.futured.showcase.data.entity.Candidate
import app.futured.showcase.data.repository.CandidateRepository
import app.futured.showcase.service.CandidateService
import app.futured.showcase.service.dto.CreateCandidateDto
import org.assertj.core.internal.bytebuddy.utility.RandomString
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import kotlin.random.Random

class CandidateServiceImplTest {
    private val candidateRepository = Mockito.mock(CandidateRepository::class.java)
    private val candidateService: CandidateService = CandidateServiceImpl(candidateRepository)

    @Test
    fun `create candidate unit test`() {
        val createDto = CreateCandidateDto(
            name = RandomString.make(),
            surname = RandomString.make(),
            email = "andy@summer.com"
        )
        val mappedEntity = Candidate(
            name = createDto.name,
            surname = createDto.surname,
            email = createDto.email
        )
        val savedCandidate = Candidate(
            id = Random(7).nextLong(),
            name = RandomString.make(),
            surname = RandomString.make(),
            email = "andy2@summer2.com",
            phone = "732037340"
        )

        `when`(candidateRepository.save(any(Candidate::class.java))).thenReturn(savedCandidate)

        val actualResult = candidateService.createCandidate(createDto)

        verify(candidateRepository).save(any(Candidate::class.java))

        Assertions.assertEquals(savedCandidate.id, actualResult.id)
        Assertions.assertEquals(savedCandidate.name, actualResult.name)
        Assertions.assertEquals(savedCandidate.surname, actualResult.surname)
        Assertions.assertEquals(savedCandidate.email, actualResult.email)
        Assertions.assertEquals(savedCandidate.phone, actualResult.phone)
    }
}
